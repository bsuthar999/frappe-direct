import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { TokenCacheService } from '../../auth/entities/token-cache/token-cache.service';
import { TOKEN } from '../../constants/app-strings';
import { switchMap } from 'rxjs/operators';
import { of, from } from 'rxjs';

@Injectable()
export class TokenGuard implements CanActivate {
  constructor(private readonly tokenCacheService: TokenCacheService) {}
  canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const accessToken = this.getAccessToken(req);
    return from(this.tokenCacheService.findOne({ accessToken })).pipe(
      switchMap(cachedToken => {
        if (!cachedToken) {
          return of(false);
        } else if (Math.floor(new Date().getTime() / 1000) < cachedToken.exp) {
          req[TOKEN] = cachedToken;
          return of(true);
        } else {
          from(
            this.tokenCacheService.deleteMany({
              accessToken: cachedToken.accessToken,
            }),
          ).subscribe({
            next: removed => {},
            error: error => {},
          });
          return of(false);
        }
      }),
    );
  }

  getAccessToken(request) {
    if (!request.headers.authorization) {
      if (!request.query.access_token) return null;
    }
    return (
      request.query.access_token ||
      request.headers.authorization.split(' ')[1] ||
      null
    );
  }
}
