import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { Observable, from } from 'rxjs';
import { ServerSettingsService } from '../../entities/server-settings/server-settings.service';
import { ServerSettings } from '../../entities/server-settings/server-settings.entity';
import {
  BEARER_HEADER_VALUE_PREFIX,
  APPLICATION_JSON_CONTENT_TYPE,
  AUTHORIZATION,
  CONTENT_TYPE_HEADER_KEY,
} from '../../../constants/app-strings';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';

@Injectable()
export class SettingsService extends AggregateRoot {
  constructor(private readonly serverSettingsService: ServerSettingsService) {
    super();
  }

  find(): Observable<ServerSettings> {
    const settings = this.serverSettingsService.find();
    return from(settings);
  }

  update(query, params) {
    return from(this.serverSettingsService.update(query, params));
  }

  getHeaders(token: TokenCache) {
    const headers = {};
    headers[
      AUTHORIZATION
    ] = `${BEARER_HEADER_VALUE_PREFIX} ${token.accessToken}`;
    headers[CONTENT_TYPE_HEADER_KEY] = APPLICATION_JSON_CONTENT_TYPE;
    return headers;
  }
}
