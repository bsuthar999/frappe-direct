import { Test, TestingModule } from '@nestjs/testing';
import { DirectController } from './direct.controller';
import { DirectService } from '../../aggregates/direct/direct.service';

describe('Direct Controller', () => {
  let controller: DirectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DirectController],
      providers: [{ provide: DirectService, useValue: {} }],
    }).compile();

    controller = module.get<DirectController>(DirectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
