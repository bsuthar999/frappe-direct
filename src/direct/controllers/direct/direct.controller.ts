import { Controller, Post, Body, Get, Query, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { DirectService } from '../../aggregates/direct/direct.service';

@Controller('direct')
export class DirectController {
  constructor(private readonly direct: DirectService) {}

  @Post('webhook/v1/add_token')
  async hookAddToken(@Body() payload) {
    return await this.direct.hookAddToken(payload);
  }

  @Post('webhook/v1/delete_token')
  async hookDeleteToken(@Body() payload) {
    return await this.direct.hookDeleteToken(payload);
  }

  @Post('webhook/v1/update_token')
  async hookUpdateToken(@Body() payload) {
    return await this.direct.hookUpdateToken(payload);
  }

  @Get('callback')
  oauth2callback(
    @Req() req: Request,
    @Res() res: Response,
    @Query('state') state: string,
    @Query('code') code: string,
  ) {
    this.direct.oauth2callback(res, state, code);
  }
}
